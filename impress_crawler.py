import csv
import datetime
import time
from enum import Enum
from typing import List, Optional

import pandas as pd
import requests
from bs4 import BeautifulSoup
from pandas import DataFrame
from pydantic import BaseModel

from data_finder import DataFinder

pd.options.mode.chained_assignment = None


class CompanyData(BaseModel):
    url: str
    company_name: Optional[str] = None
    street: Optional[str] = None
    city: Optional[str] = None
    phone: List[str] = []
    email: List[str] = []


class ErrorType(Enum):
    IMPRESS_NOT_FOUND = "IMPRESS_NOT_FOUND"
    PAGE_NOT_REACHABLE = "PAGE_NOT_REACHABLE"

    PHONE_NOT_FOUND = "PHONE_NOT_FOUND"
    STREET_NOT_FOUND = "STREET_NOT_FOUND"
    CITY_NOT_FOUND = "CITY_NOT_FOUND"
    EMAIL_NOT_FOUND = "EMAIL_NOT_FOUND"


def log_error(link: str, type: ErrorType):
    timestamp = datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S")
    print(f"[{timestamp}] {str(type)} for {link}")
    with open("errors.csv", 'a+', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow([timestamp, str(type), link])


def matches_impress(href):
    possible_impress = ['impress', 'imprint']
    return any(item in href.lower() for item in possible_impress)


def prepare_content(soup: BeautifulSoup) -> List[str]:
    return [t.strip() for t in soup.strings if len(t.strip()) > 0]


def crawl_impress(found_link: str):
    response = requests.get(found_link, timeout=10)
    if response.status_code == 200:
        soup = BeautifulSoup(response.content, 'html.parser')

        prepared_content = prepare_content(soup)
        phone = DataFinder.find_phone(prepared_content)
        city = DataFinder.find_city(prepared_content)
        street = DataFinder.find_street(prepared_content)
        email = DataFinder.find_email(prepared_content)
        fax = DataFinder.find_fax(prepared_content)
        person = DataFinder.find_person(prepared_content)
        company_name = DataFinder.find_company_name(prepared_content)

        if len(street) == 0:
            log_error(link=url, type=ErrorType.STREET_NOT_FOUND)
        if len(city) == 0:
            log_error(link=url, type=ErrorType.CITY_NOT_FOUND)
        if len(phone) == 0:
            log_error(link=url, type=ErrorType.PHONE_NOT_FOUND)
        if len(email) == 0:
            log_error(link=url, type=ErrorType.EMAIL_NOT_FOUND)

        return CompanyData(
            street=street[0].strip() if street else None,
            city=city[0].strip() if city else None,
            phone=[p.strip() for p in phone] if phone else [],
            email=[e.strip() for e in email] if email else [],
            url=url
        )

    else:
        log_error(link=url, type=ErrorType.PAGE_NOT_REACHABLE)
        return None


def get_impress_link(url):
    response = requests.get(url)
    if response.status_code == 200:
        soup = BeautifulSoup(response.content, 'html.parser')
        impress_links = soup.find_all('a', string=lambda text: text and matches_impress(text))
        if not impress_links:
            impress_links = soup.find_all('a', attrs={"aria-label": True})
            impress_links = [link for link in impress_links if matches_impress(link['aria-label'].strip())]
        if not impress_links:
            impress_links = soup.find_all('a', href=lambda href: href and matches_impress(href))
        if impress_links:
            for link in impress_links:
                found_link = link['href']
                if not ("http://" in link['href'] or "https://" in link['href']):
                    if link['href'][0] == "/":
                        found_link = url[:-1] + link['href'] if url[-1] == "/" else url + link['href']
                    else:
                        found_link = url + link['href'] if url[-1] == "/" else url[:-1] + link['href']
                return found_link
        else:
            log_error(link=url, type=ErrorType.IMPRESS_NOT_FOUND)
            return None
    else:
        log_error(link=url, type=ErrorType.PAGE_NOT_REACHABLE)
        return None


def load_csv():
    return pd.read_csv('data.csv')


def get_data(row: DataFrame) -> Optional[CompanyData]:
    link = get_impress_link(row['URL'].iloc[0])
    if not link:
        return None
    return crawl_impress(link)


if __name__ == "__main__":
    content = load_csv()
    for index in content.index:
        if index < 733:
            continue
        url = content['URL'][index]
        company_data = get_data(content.iloc[[index]])

        if company_data is not None:
            content['Adresszeile'][index] = company_data.street
            content['Postleitzahl und Ort'][index] = company_data.city
            content['Email address'][index] = company_data.email
            content['Phone number'][index] = company_data.phone
            content.iloc[[index]].to_csv('results.csv', mode='a+', index=False, header=False)

        time.sleep(2)
