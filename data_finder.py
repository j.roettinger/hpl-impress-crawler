import re
from enum import Enum
from typing import List, Optional

PHONE_PREFIX = ["Tel ", "Tel:", "Tel.:", "Telefon:", "Telephon:", "Phone:"]
FAX_PREFIX = ["Fax ", "Fax:", "Fax.:", "Telefax:"]
PERSON_PREFIX = ["Inhaltlich Verantwortlicher", "Geschäftsführer", "Inhaber", "Verantwortliche", "Vertreten durch",
                 "Vertretungsberechtigte Geschäftsführer"]
COMPANY_SUFFIX = ["AG", "GbR", "GmbH", "UG", "(haftungsbeschränkt)"]


class RegexType(Enum):
    PHONE_NUMBER = r"[()+ /\d\-]{5,}"
    EMAIL_ADDRESS = r"[\w\_\-\.]+@[\w\_\-\.]+\.[A-Za-z]{2,4}"
    CITY_NAME = r"\d{5} [A-Za-zöäüÖÄÜß\-\/]+"
    STREET_NAME = r"[A-Za-zöäüÖÄÜß\.\-\ ]+ \d+[A-Za-z\-]*"


class DataFinder:

    @staticmethod
    def find_phone(content: List[str]) -> Optional[str]:
        for line in content:
            for prefix in PHONE_PREFIX:
                if line.strip().lower().startswith(prefix.lower()):
                    try:
                        phone_number = re.findall(re.compile(RegexType.PHONE_NUMBER.value), line.strip())[0]
                        return phone_number.strip()
                    except IndexError:
                        continue
        for line in content:
            try:
                phone_number = re.findall(re.compile(RegexType.PHONE_NUMBER.value), line.strip())[0]
                return phone_number.strip()
            except IndexError:
                continue
        return None

    @staticmethod
    def find_city(content: List[str]) -> Optional[str]:
        for line in content:
            result = re.findall(re.compile(RegexType.CITY_NAME.value), line.strip())
            if result:
                return result[0]
        return None

    @staticmethod
    def find_street(content: List[str]) -> Optional[str]:
        for line in content:
            result = re.findall(re.compile(RegexType.STREET_NAME.value), line.strip())
            if result:
                return result[0]
        return None

    @staticmethod
    def find_email(content: List[str]) -> Optional[str]:
        for line in content:
            result = re.findall(re.compile(RegexType.EMAIL_ADDRESS.value), line.strip())
            if result:
                return result[0]
        return None

    @staticmethod
    def find_fax(content: List[str]) -> Optional[str]:
        for line in content:
            for prefix in FAX_PREFIX:
                if line.strip().lower().startswith(prefix.lower()):
                    try:
                        fax_number = re.findall(re.compile(RegexType.PHONE_NUMBER.value), line.strip())[0]
                        return fax_number.strip()
                    except IndexError:
                        continue
        return None

    @staticmethod
    def find_person(content: List[str]) -> Optional[str]:
        for line in content:
            for prefix in PERSON_PREFIX:
                if line.strip().lower().startswith(prefix.lower()):
                    try:
                        person = line.strip().split(prefix)[1]
                        return person.strip()
                    except IndexError:
                        return line.strip()
        return None


    @staticmethod
    def find_company_name(content: List[str]) -> Optional[str]:
        for line in content:
            for suffix in COMPANY_SUFFIX:
                if line.strip().endswith(suffix):
                    return line.strip()
        return None

    @staticmethod
    def find_elements(type: RegexType, string: str):
        pattern = re.compile(pattern=type.value, flags=re.MULTILINE)
        return re.findall(pattern=pattern, string=string)
